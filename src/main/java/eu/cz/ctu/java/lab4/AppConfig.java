package eu.cz.ctu.java.lab4;

import eu.cz.ctu.java.lab4.services.LoggerService;
import eu.cz.ctu.java.lab4.services.MailService;
import eu.cz.ctu.java.lab4.services.SlackService;
import org.springframework.context.annotation.Bean;

public class AppConfig {
    @Bean
    public Logger logger(){
        return new Logger(loggerService());
    }

    @Bean
    public LoggerService loggerService() {
        return new MailService();
    }
}
