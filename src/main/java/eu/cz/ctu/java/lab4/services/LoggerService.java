package eu.cz.ctu.java.lab4.services;

import eu.cz.ctu.java.lab4.Level;

public interface LoggerService {

    void log(String msg, Level level);

    void debug(String msg);

    void info(String msg);

    void warn(String msg);

    void error(String msg);

}
